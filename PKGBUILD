# Maintainer: Mark Wagie <mark at manjaro dot org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Contributor: Helmut Stult
# Contributor: Johannes Löthberg <johannes@kyriasis.com>

pkgname=pacutils
pkgver=0.11.1
pkgrel=3
pkgdesc='Helper tools for libalpm'
url='https://github.com/andrewgregory/pacutils'
arch=('x86_64' 'aarch64')
license=('MIT')
depends=(
  'glibc'
  'libarchive'
  'pacman'
)
makedepends=('git')
_commit=aa4ba003cb446907a71be4aa976546d113ecc688  # tags/v0.11.1^0
source=("git+https://github.com/andrewgregory/pacutils.git#commit=${_commit}"
        'SyncFirst.patch')
sha256sums=('SKIP'
            '77a46d1dcd1a4d86033e36cd02b9b9dd171a4b5d80ef13755fec80a7f9e6ccd2')
#validpgpkeys=('0016846EDD8432261C62CB63DF3891463C352040')  # Andrew Gregory <andrew.gregory.8@gmail.com>

pkgver() {
  cd pacutils
  git describe --tags | sed 's/^v//;s/-/+/g'
}

prepare() {
  cd pacutils
  for commit in a8cb36d865a74ade470b6e71b434fc8b0e981e04 \
                9788717c8798b6accae0090a9bad49b504781f0d \
                045b55113c766efef01d636e48b37d3a1ead16a1; do
    git show "$commit" | git apply -
  done

  patch -p1 -i ../SyncFirst.patch
}

build() {
  cd pacutils
  make CFLAGS="$CFLAGS $LDFLAGS" SYSCONFDIR=/etc LOCALSTATEDIR=/var
}

check() {
  cd pacutils
  make check
}

package() {
  cd pacutils
  make DESTDIR="$pkgdir" PREFIX=/usr install
  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/"$pkgname"/COPYING
}

# vim: set ft=PKGBUILD et sw=2:

